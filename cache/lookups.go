package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/mailgun/groupcache/v2"
	"groupcachetest/entities"
	"log"
	"time"
)

type lookups struct {
	users    *groupcache.Group
	products *groupcache.Group
}

type usersStore interface {
	GetById(id string) (entities.User, error)
}
type productsStore interface {
	GetById(id string) (entities.Product, error)
}

func NewLookups(users usersStore, products productsStore) *lookups {
	usersGroup := groupcache.NewGroup("users", 3000000, groupcache.GetterFunc(
		func(ctx context.Context, id string, dest groupcache.Sink) error {
			log.Println("Getting user from store")
			user, err := users.GetById(id)
			if err != nil {
				return fmt.Errorf("error getting user with id %s from store: %w", id, err)
			}
			b, err := json.Marshal(user)
			if err := dest.SetBytes(b, time.Now().Add(time.Hour*1)); err != nil {
				return err
			}
			return nil
		},
	))
	productsGroup := groupcache.NewGroup("products", 3000000, groupcache.GetterFunc(
		func(ctx context.Context, id string, dest groupcache.Sink) error {
			log.Println("Getting product from store")
			product, err := products.GetById(id)
			if err != nil {
				return fmt.Errorf("error getting product with id %s from store: %w", id, err)
			}
			b, err := json.Marshal(product)
			if err := dest.SetBytes(b, time.Now().Add(time.Hour*1)); err != nil {
				return err
			}
			return nil
		},
	))
	return &lookups{users: usersGroup, products: productsGroup}
}

func (c lookups) GetUser(id string) (entities.User, error) {
	var userByteView groupcache.ByteView
	if err := c.users.Get(context.Background(), id, groupcache.ByteViewSink(&userByteView)); err != nil {
		return entities.User{}, fmt.Errorf("could not retrieve user with id %s from cache", id)
	}
	var user entities.User
	if err := json.Unmarshal(userByteView.ByteSlice(), &user); err != nil {
		return entities.User{}, fmt.Errorf("could not unmarshal bytes from cache for user %s: %w", id, err)
	}
	return user, nil
}

func (c lookups) GetProduct(id string) (entities.Product, error) {
	var productByteView groupcache.ByteView
	if err := c.users.Get(context.Background(), id, groupcache.ByteViewSink(&productByteView)); err != nil {
		return entities.Product{}, fmt.Errorf("could not retrieve product with id %s from cache", id)
	}
	var product entities.Product
	if err := json.Unmarshal(productByteView.ByteSlice(), &product); err != nil {
		return entities.Product{}, fmt.Errorf("could not unmarshal bytes from cache for product %s: %w", id, err)
	}
	return product, nil
}
