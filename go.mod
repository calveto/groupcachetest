module groupcachetest

go 1.18

require github.com/mailgun/groupcache/v2 v2.3.0

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/segmentio/fasthash v1.0.3 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/sys v0.0.0-20220330033206-e17cdc41300f // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
