package entities

type Product struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
