package main

import (
	"github.com/mailgun/groupcache/v2"
	"groupcachetest/cache"
	"groupcachetest/config"
	"groupcachetest/handlers"
	"groupcachetest/repositories"
	"log"
	"net/http"
)

func requestLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		log.Println(request.Method, request.Host, request.RequestURI)
		next.ServeHTTP(writer, request)
	})
}

func failOnErr(err error) {
	if err != nil {
		log.Fatalf(err.Error())
	}
}

func main() {
	// Validate arguments ex. $ ./groupcachetest 8080 localhost:8081,localhost:8082
	conf, err := config.ParseCLIParams()
	failOnErr(err)
	// Create groupcache pool
	pool := groupcache.NewHTTPPoolOpts(conf.CurrentHost, &groupcache.HTTPPoolOptions{})
	pool.Set(conf.SiblingNodes...)
	// Create program components
	users := repositories.NewUsersRepository()
	products := repositories.NewProductsRepository()
	lookups := cache.NewLookups(users, products)
	// Create server and bind http handlers
	r := http.NewServeMux()
	r.HandleFunc("/users/", handlers.UserHandler(lookups))
	r.HandleFunc("/products/", handlers.ProductHandler(lookups))
	r.Handle("/_groupcache/", pool)
	server := http.Server{
		Addr:    conf.HostBinding,
		Handler: requestLogger(r),
	}
	log.Printf("Listening for request on %s\n", conf.HostBinding)
	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
