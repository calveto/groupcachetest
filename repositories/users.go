package repositories

import (
	"fmt"
	"groupcachetest/entities"
	"log"
	"time"
)

type users struct {
	users map[string]entities.User
}

func NewUsersRepository() *users {
	return &users{
		users: map[string]entities.User{
			"1": {
				ID:   "1",
				Name: "User 1",
			},
			"2": {
				ID:   "2",
				Name: "User 2",
			},
			"3": {
				ID:   "3",
				Name: "User 3",
			},
		},
	}
}

func (r users) GetById(id string) (entities.User, error) {
	log.Println("Getting user", id, "from database")
	time.Sleep(2 * time.Second)
	u, ok := r.users[id]
	if ok {
		return u, nil
	}
	return entities.User{}, fmt.Errorf("user not found")
}
