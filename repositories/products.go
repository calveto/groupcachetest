package repositories

import (
	"fmt"
	"groupcachetest/entities"
	"log"
	"time"
)

type products struct {
	products map[string]entities.Product
}

func NewProductsRepository() *products {
	return &products{
		products: map[string]entities.Product{
			"1": {
				ID:   "1",
				Name: "Product 1",
			},
			"2": {
				ID:   "2",
				Name: "Product 2",
			},
			"3": {
				ID:   "3",
				Name: "Product 3",
			},
		},
	}
}

func (r products) GetById(id string) (entities.Product, error) {
	log.Println("Getting product", id, "from database")
	time.Sleep(2 * time.Second)
	u, ok := r.products[id]
	if ok {
		return u, nil
	}
	return entities.Product{}, fmt.Errorf("product not found")
}
