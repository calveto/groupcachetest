package config

import (
	"fmt"
	"os"
	"strings"
)

type Conf struct {
	CurrentHost  string
	HostBinding  string
	SiblingNodes []string
}

func ParseCLIParams() (*Conf, error) {
	if len(os.Args) < 2 {
		return nil, fmt.Errorf("add port when running command ex. `$ ./groupcachetest 8080")
	}
	port := os.Args[1]
	hostBinding := fmt.Sprintf("localhost:%s", port)
	currentHost := fmt.Sprintf("http://%s", hostBinding)
	siblingNodes := []string{currentHost}
	if len(os.Args) > 2 {
		for _, host := range strings.Split(os.Args[2], ",") {
			hostUrl := host
			if !strings.Contains(host, "http") {
				hostUrl = fmt.Sprintf("http://%s", host)
			}
			siblingNodes = append(siblingNodes, hostUrl)
		}
	}
	return &Conf{
		CurrentHost:  currentHost,
		HostBinding:  hostBinding,
		SiblingNodes: siblingNodes,
	}, nil
}
