package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

func UserHandler(lookups userCache) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		id := strings.TrimPrefix(request.URL.Path, "/users/")
		user, err := lookups.GetUser(id)
		if err != nil {
			http.Error(writer, "cannot get user", http.StatusInternalServerError)
			return
		}
		log.Printf("User: %+v\n", user)
		if err := json.NewEncoder(writer).Encode(user); err != nil {
			msg := "could not marshal as json"
			log.Printf("%s: %s", msg, err.Error())
			http.Error(writer, msg, http.StatusInternalServerError)
		}
	}
}
