package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

func ProductHandler(lookups productCache) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		id := strings.TrimPrefix(request.URL.Path, "/products/")
		product, err := lookups.GetProduct(id)
		if err != nil {
			http.Error(writer, "cannot get product", http.StatusInternalServerError)
			return
		}
		log.Printf("Product: %+v\n", product)
		if err := json.NewEncoder(writer).Encode(product); err != nil {
			msg := "could not marshal as json"
			log.Printf("%s: %s", msg, err.Error())
			http.Error(writer, msg, http.StatusInternalServerError)
		}
	}
}
