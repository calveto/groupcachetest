package handlers

import "groupcachetest/entities"

type userCache interface {
	GetUser(id string) (entities.User, error)
}

type productCache interface {
	GetProduct(id string) (entities.Product, error)
}
