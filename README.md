# Golang groupcache Test Project

Small testing project to prove out some concepts when using the [groupcache](https://github.com/mailgun/groupcache) library

### How to run

1. Make sure you have go 1.18 install (https://go.dev/)
2. Create a few instances of the program by run it with the following params `$ go run main.go <port: ex. 8080> <sibling nodes: ex. localhost:8081>`
3. Example run:

Terminal one: `$ go run main.go 8080 localhost:8081`

Terminal two: `$ go run main.go 8081 localhost:8080`
